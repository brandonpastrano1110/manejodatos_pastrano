/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ManejoDatos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener; 
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import models.DbProductos;
import vista.dlgManejo;
import models.Productos;

public class controlador implements ActionListener{
    private dlgManejo vista;
    private DbProductos dbPro;
    private Productos pro;

    public controlador(dlgManejo vista, DbProductos dbPro, Productos pro) {
        this.vista = vista;
        this.dbPro=dbPro;
        this.pro=pro;
        
        //HABILITADO
        vista.btnNuevoHa.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnBuscarHa.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnDeshabiliar.addActionListener(this);
        vista.btnGuardarHa.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        //DESHABILITADO
        vista.btnHabilitar.addActionListener(this);
        vista.btnBuscarDe.addActionListener(this);
        
    }
    public void mosTabla(){
        try {
            vista.jtbHab.setModel(dbPro.listar());
            vista.jtbDesh.setModel(dbPro.listarD());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(vista, "no se pudo mostrar la tabla "+e.getMessage());
        }
    }
    public void IniciarVista(){
        mosTabla();
        vista.setSize(700,700);
        vista.setTitle("MANEJO DE DATOS");
        vista.setVisible(true);
        
    }
    public void limpiar(){
        vista.txtCodigoHa.setText(""); 
        vista.txtNombreHa.setText("");
        vista.txtPrecioHa.setText("");
    } 
    public void fecha(){
        int mes = vista.jdateFecha.getCalendar().get(Calendar.MONTH) + 1;
        int dia = vista.jdateFecha.getCalendar().get(Calendar.DAY_OF_MONTH);
        int año = vista.jdateFecha.getCalendar().get(Calendar.YEAR);
        String fecha;
        if (mes < 10) {
            fecha = String.format("%d,0%d,%d", año, mes, dia);
        } else {
            fecha = String.format("%d,%d,%d", año, mes, dia);
        }
        pro.setFecha(fecha);
    }
    public void mFecha(){
        try {
            Date fecha;
            fecha=forma.parse(pro.getFecha());
            vista.jdateFecha.setDate(fecha);
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(vista, "No se pudo Buscar, surgio el siguiente error: "+e.getMessage());
        }
        
    }
    DateFormat forma = new SimpleDateFormat("yyyy-MM-dd");
    
    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        DbProductos dbPro = new DbProductos();
        Productos pro = new Productos();
        dlgManejo vista = new dlgManejo(new JFrame(), true);
        controlador contra = new controlador(vista, dbPro, pro);
        
        contra.IniciarVista();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==vista.btnNuevoHa){
            vista.btnBuscarHa.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.btnDeshabiliar.setEnabled(true);
            vista.btnGuardarHa.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            
            vista.txtCodigoHa.setEnabled(true);
            vista.jdateFecha.setEnabled(true);
            vista.txtNombreHa.setEnabled(true);
            vista.txtPrecioHa.setEnabled(true); 
        }
        if(e.getSource()==vista.btnCancelar){
            vista.btnBuscarHa.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            vista.btnDeshabiliar.setEnabled(false);
            vista.btnGuardarHa.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            
            vista.txtCodigoHa.setEnabled(false);
            vista.jdateFecha.setEnabled(false);
            vista.txtNombreHa.setEnabled(false);
            vista.txtPrecioHa.setEnabled(false);
            limpiar();
        }
        if(e.getSource()==vista.btnLimpiar){
            limpiar();
        }
        if(e.getSource()==vista.btnGuardarHa){
            try {
                if(dbPro.isExiste(vista.txtCodigoHa.getText())){
                    pro.setCodigo(vista.txtCodigoHa.getText()); 
                    pro.setNombre(vista.txtNombreHa.getText());
                    pro.setPrecio(Float.parseFloat(vista.txtPrecioHa.getText()));
                    pro.setStatus(0);
                    fecha();
                    try {
                        dbPro.insertar(pro);
                        dbPro.actualizar(pro);
                        JOptionPane.showMessageDialog(vista, "Se guardo con exito");

                    } catch (Exception e1) {
                        JOptionPane.showMessageDialog(vista,  "Surgio el siguiente error: "+e1.getMessage());
                    }
                }else{
                    JOptionPane.showMessageDialog(vista, "EL CODIGO YA EXISTE");
                }
                
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex2.getMessage());
            }            
            mosTabla();
            
            //dbPro.insertar(pro);
        }
        
        if(e.getSource()==vista.btnBuscarHa){
            
            try{
                pro = (Productos) dbPro.buscar(vista.txtCodigoHa.getText());
                pro.setCodigo(vista.txtCodigoHa.getText());
                limpiar();
                dbPro.buscar(vista.txtCodigoHa.getText());
                vista.txtCodigoHa.setText(pro.getCodigo());
                vista.txtPrecioHa.setText(String.valueOf(pro.getPrecio())); 
                vista.txtNombreHa.setText(pro.getNombre()); 
                if(dbPro.buscar(vista.txtCodigoHa.getText()).equals(-1)){
                    JOptionPane.showMessageDialog(vista, "No se encontro");
                }
                else{
                    JOptionPane.showMessageDialog(vista, "Se encontro");
                    vista.txtCodigoHa.setText(pro.getCodigo());
                   vista.txtPrecioHa.setText(String.valueOf(pro.getPrecio())); 
                   vista.txtNombreHa.setText(pro.getNombre());
                   mFecha();
                }
                
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "No se pudo Buscar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
            
        }
        if(e.getSource()==vista.btnBuscarDe){
            try{
                pro = (Productos) dbPro.buscarD(vista.txtCodigoDe.getText());
                pro.setCodigo(vista.txtCodigoDe.getText());
                limpiar();
                dbPro.buscar(vista.txtCodigoDe.getText());
                vista.txtCodigoDe.setText(pro.getCodigo()); 
                vista.txtNombreDe.setText(pro.getNombre());
                
                if(dbPro.buscarD(vista.txtCodigoDe.getText()).equals(-1)){
                    JOptionPane.showMessageDialog(vista, "No se encontro");
                }
                else
                    JOptionPane.showMessageDialog(vista, "Se encontro");
                
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "No se pudo Buscar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
            
        }
        if(e.getSource()==vista.btnDeshabiliar){
            try {
                if(dbPro.deshabilitar(pro)){
                    dbPro.actualizar(pro);
                    JOptionPane.showMessageDialog(vista, "Se deshabilito con exito");
                }
                else{
                    JOptionPane.showMessageDialog(vista, "No se pudo deshabilitar");
                }
                mosTabla();
             }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex2.getMessage());
            }
            limpiar();
        } 
        if(e.getSource()==vista.btnHabilitar){
            try {
                if(dbPro.habilitar(pro)){
                    dbPro.actualizar(pro);
                    JOptionPane.showMessageDialog(vista, "Se habilito con exito");
                }
                else{
                    JOptionPane.showMessageDialog(vista, "No se pudo habilitar");
                }
                mosTabla();
             }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex2.getMessage());
            }
            limpiar();
        }
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"¿Deseas salir?",
            "Decide", JOptionPane.YES_NO_OPTION);
             if(option==JOptionPane.YES_NO_OPTION){
                 vista.dispose();
                 System.exit(0);
             }
        }
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
